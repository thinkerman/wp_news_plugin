<?php
/*
Plugin Name: Wp-news
Version: 0.1
Plugin URI: http://devcodes.xyz
Description: A news plugin for your website
Author: Samuel
Author URI: http://www.devcodes.xyz?about=all
*/
class Wpnp{
	private static $ins = null;
	public static function instance(){
		is_null(self::$ins) ? self::$ins = new Wpnp(): null;
		return self::$ins;
	}
	public static function init(){
		add_action( "wp_enqueue_scripts", array(self::instance(),"wpnp_frontend_assets"));
		add_action( "wp_ajax_nopriv_news_api_call", array(self::instance(),"news_api_call"));
		add_action( "wp_ajax_news_news_api_call", array(self::instance(), "news_api_call"));


		add_action( "wp_ajax_nopriv_wpnp_save_news", array(self::instance(),"wpnp_save_news"));
		add_action( "wp_ajax_wpnp_save_news", array(self::instance(),"wpnp_save_news"));

		add_action("admin_menu", array(self::instance(),"wpnp_admin_menu"));
		add_action("admin_init", array(self::instance(),"wpnp_settings_init"));

		add_shortcode( "wp_news_plugin", array(self::instance(), "wpnp_news_shortcode") );
		add_shortcode( "wpnp_profile_plugin", array(self::instance(), "wpnp_profile_shortcode") );

		// admin
		add_action( 'admin_enqueue_scripts', array(self::instance(),'wpnp_admin_assets_styles'));
		add_action( 'admin_enqueue_scripts', array(self::instance(),'wpnp_admin_assets_js'));
		add_action('wpnp_ajax_wpnp_admin_assets_js', array(self::instance(), 'wpnp_admin_assets_js'));
		register_activation_hook( __FILE__, array(self::instance(),'wpnp_database'));

	}
	public static function wpnp_settings_init(){
		register_setting('wpnp_plugin', 'wpnp_settings');
		add_settings_section( "wpnp_settings_section", "Wordpress News Plugin", array(self::instance(), "wpnp_settings_section_callback"), "wpnp_plugin" );
		add_settings_section( "wpnp_settings_section_developers", "Developers' section", array(self::instance(), "wpnp_settings_section_developers_callback"), "wpnp_plugin" );

		add_settings_field( "wpnp_api_key", "Enter API Key here", array(self::instance(), "wpnp_settings_section_apikey_callback"), "wpnp_plugin", "wpnp_settings_section");
		add_settings_field( "wpnp_number_of_posts", "How many posts to be loaded?", array(self::instance(), "wpnp_settings_section_num_of_posts_callback"), "wpnp_plugin", "wpnp_settings_section");
		add_settings_field( "wpnp_user_articles", "URL of user profile page", array(self::instance(), "wpnp_settings_section_user_profile"), "wpnp_plugin", "wpnp_settings_section");
//    add_settings_field( "wpnp_allow_save", "Allow users save Articles", array(self::instance(), "wpnp_settings_section_allow_save"), "wpnp_plugin", "wpnp_settings_section");
//    add_settings_field( "wpnp_allow_share", "Allow users share Articles", array(self::instance(), "wpnp_settings_section_allow_share"), "wpnp_plugin", "wpnp_settings_section");

		add_settings_field( "wpnp_developers", "List of Action hooks", array(self::instance(), "wpnp_developers"), "wpnp_plugin", "wpnp_settings_section_developers");


	}
	public static function wpnp_frontend_assets(){
		$options = get_option('wpnp_settings');
		wp_enqueue_style( "wpnp-styles", plugin_dir_url( __FILE__ )."incs/assets/styles/comp/style.css");
		wp_enqueue_style( "font-awesome", "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css");
		wp_enqueue_script( "wpnp-script", plugin_dir_url( __FILE__ )."incs/assets/js/main.js" , "jquery");
		wp_enqueue_script( "wpnp-fa-script", "https://use.fontawesome.com/5c40ac4e6b.js" , "jquery");
		wp_localize_script( "wpnp-script", "wpnp", array(
			"ajax_url" => admin_url('admin-ajax.php'),
			"api_function" => "news_api_call",
			"apiKey" => "56a7f1dcdf9c488389bc8775f5fc22ec",
			"placeholder_image" => plugin_dir_url( __FILE__ )."assets/img/featuredimg.png",
			"post_per_page" => $options['wpnp_number_of_posts'],
			"author_icon" => '<i class="fa fa-user fa-lg" aria-hidden="true"></i>',
			"title_icon" => '<i class="fa fa-book" aria-hidden="true"></i>',
			"twitter_icon" => '<i class="fa fa-twitter fa-lg" aria-hidden="true"></i>',
			"facebook_icon" => '<i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i>',
			"favourite_icon" => '<i class="fa fa-thumbs-up fa-lg" aria-hidden="true"></i>',
			"unlike_icon" => '<i class="fa fa-thumbs-down" aria-hidden="true"></i>',
			"published_icon" => '<i class="fa fa-calendar fa-lg" aria-hidden="true"></i>',
			"source_icon" => '<i class="fa fa-internet-explorer fa-lg" aria-hidden="true"></i>',
			"spinner_img" => '<p><i class="fa fa-spinner fa-lg fa-spin"></i> Unable to load news </p>',
			"user_id" => is_user_logged_in() ? get_current_user_id() : "not logged in",
			"fav_func" => 'wpnp_save_news',
			"del_func" => 'wpnp_del_news',
//      "user_save" => $options['wpnp_allow_save'],
			"user_profile_url" => $options['wpnp_user_articles']
		) );
	}
	public static function wpnp_admin_assets_styles($hook){
		if($hook != 'toplevel_page_wpnp_plugin') {return;}
		wp_enqueue_style( 'wpnp_admin_css', plugin_dir_url( __FILE__ )."incs/assets/styles/wpnp_admin.css" );
	}
	public static function wpnp_admin_assets_js($hook){
		if($hook != 'toplevel_page_wpnp_plugin') {return;}
		wp_enqueue_script( 'wpnp-admin-js', plugin_dir_url( __FILE__ )."incs/assets/js/admin.js", 'jquery');
		wp_localize_script( 'wpnp-admin-js', 'wpnpAdmin', array(
			"page" => $hook,
		));
	}
	public static function wpnp_news_shortcode($hook){
		ob_start();
		include ('incs/core_files/wpnp_news_shortcodes_html.php');
		return ob_get_clean();
	}
	public static function wpnp_profile_shortcode($hook){
		ob_start();
		include ('incs/core_files/wpnp_profile_shortcodes_html.php');
		return ob_get_clean();
	}
	public static function wpnp_admin_menu(){
		add_menu_page(
			"Wpnp",
			"WP News Plugin",
			"manage_options",
			"wpnp_plugin",
			array(self::instance(),'wpnp_settings_page'),
			'dashicons-money',
			60
		);
	}
	public static function wpnp_settings_page(){
		?>
        <form action='options.php' method='post'>
            <?php settings_fields( 'wpnp_plugin' ); ?>
            <?php do_settings_sections( 'wpnp_plugin' ); ?>
            <?php submit_button(); ?>
        </form>
		<?php
	}
	public static function wpnp_settings_section_callback(){
		echo "Plugin settings";
	}
	public static function wpnp_settings_section_developers_callback(){
		echo "If you are interested in extending this plugin here are some of the hooks mixed in the pot";
	}
	public static function wpnp_developers(){
		?>
        <p>To use, simply call the  `add_action` hook in your plugin to hook into this plugin. More actions and filters on the way!</p>
        <code>
            `wpnp_after_news_container`<br>
            `wpnp_after_search`<br>
            `wpnp_after_main_cat`<br>
            `wpnp_after_mobile_cat`
        </code>
		<?
	}
	public static function wpnp_settings_section_apikey_callback(){
		$options = get_option('wpnp_settings');
		if($options == null) {
			$options = "";
		}else{
			$options = $options['wpnp_api_key'];
		}
		?>
        <input type='text' name='wpnp_settings[wpnp_api_key]' value='<?php echo $options; ?>' autocomplete="off"><br>
        <label for="wpnp_settings[wpnp_api_key]">Obtain API Key from <a href="https://www.newsapi.com">here</a>. Note: There's a limit of 1000 calls/per day for free/developer account!</label>
		<?php
	}
	public static function wpnp_settings_section_num_of_posts_callback(){
		$options = get_option('wpnp_settings');
		if($options == null) {
			$options = "";
		}else{
			$options = $options['wpnp_number_of_posts'];
		}
		?>
        <input type='text' name='wpnp_settings[wpnp_number_of_posts]' value='<?php echo $options; ?>' autocomplete="off"><br>
        <label for="wpnp_settings[wpnp_number_of_posts]">Only interger values are allowed. Default is 9 posts.</label>
		<?php
	}
	public static function wpnp_settings_section_user_profile(){
		$options = get_option('wpnp_settings');
		if($options == null) {
			$options = "";
		}else{
			$options = $options['wpnp_user_articles'];
		}
		?>
        <input type='text' name='wpnp_settings[wpnp_user_articles]' value='<?php echo $options; ?>'><br>
        <label for="wpnp_settings[wpnp_user_articles]">Please enter the URL of the page you want to direct users to for their saved articles.</label>
		<?php
	}
	public static function wpnp_settings_section_allow_share(){
		$options = sanitize_text_field(get_option('wpnp_settings'));
		echo "<pre>".print_r($options,true)."</pre>";
		?>
        <input type='checkbox' name='wpnp_settings[wpnp_allow_share]'>
		<?php
	}
	public static function wpnp_settings_section_allow_save(){
		$options = get_option('wpnp_settings');
		echo "<pre>".print_r($options,true)."</pre>";
		?>
        <input type='checkbox' name='wpnp_settings[wpnp_allow_save]'>
		<?php
	}
	public static function wpnp_database(){
		$wpnp_table = $wpdb->prefix.wpnp;
		$sql = "CREATE TABLE $wpnp_table (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      user int(9) NOT NULL,
      time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
      title varchar(800) NOT NULL,
      summary varchar(1000) NOT NULL,
      url varchar(1000) DEFAULT '' NOT NULL,
      img varchar(1000) NOT NULL,
      author varchar(200) NOT NULL,
      source varchar(1000) DEFAULT '' NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
	public static function wpnp_save_news(){
		global $wpdb;

		$table_name = $wpdb->prefix.'wpnp';

		if (is_user_logged_in()) {
			if (isset($_POST['delete'])) {
				$id = intval($_POST['id']);
				$wpdb->delete( $table_name, array( 'id' => $id ) );
				echo "Article deleted";
			}elseif (isset($_POST['savefav'])) {
				$user = intval($_POST['user_id']);
				$title = sanitize_text_field($_POST['title']);
				$time = $_POST['time'];
				$summary = sanitize_text_field($_POST['summary']);
				$url = esc_url($_POST['post_url']);
				$img = esc_url($_POST['img_url']);
				$author = sanitize_text_field($_POST['author']);
				$source = sanitize_text_field($_POST['source']);
				$sql = "SELECT count(*)  FROM $table_name  WHERE user='".$user."' AND title='".$title."'";
				$count = $wpdb->get_var($sql);
				// echo $count;
				if($count == 0 ){
					$wpdb->insert(
						$table_name,
						array(
							'user' => get_current_user_id(),
							'title' => $title,
							'time' => $time,
							'summary' => $summary,
							'url' => $url,
							'img' => $img,
							'author' => $author,
							'source' => $source
						)
					);
					echo "Article saved";
				}else{
					echo "Article already saved";
				}
			}else{
				echo "Please login to save articles";
			}
		}
		wp_die();
	}
	public function test_action(){
		echo "Hello world";
	}
}
Wpnp::init();

?>
