
<div id="wpnp-container">
  <div id="wpnp-search">
    <input id="wpnp-search-input" type="text" name="wpnp-search" value="" placeholder="Search for keyword..">
    <div id="wpnp-clear"></div>
  </div>
  <?php do_action('wpnp_after_search'); ?>
  <div class="wpnp-news-info"><p></p></div>
  <div id='wpnp-country-wrapper'>
    <select id='wpnp-country-select'></select>
    <ul class="wpnp-cat">
      <li class="wpnp-cat-li" id="wpnp-general" data-api="general">World</li>
      <li class="wpnp-cat-li cat-active" id="wpnp-politics" data-api="politics">Politics</li>
      <li class="wpnp-cat-li" id="wpnp-business" data-api="business">Business</li>
      <li class="wpnp-cat-li" id="wpnp-entertainment" data-api="entertainment">Entertainment</li>
      <li class="wpnp-cat-li" id="wpnp-sports" data-api="sports">Sports</li>
      <li class="wpnp-cat-li" id="wpnp-health" data-api="health">Health</li>
      <li class="wpnp-cat-li" id="wpnp-science" data-api="science">Science</li>
      <li class="wpnp-cat-li" id="wpnp-tech" data-api="technology">Technology</li>
      <li class="wpnp-cat-li" id="wpnp-user-profile" style="text-decoration: underline;">My Articles</li>
    </ul>
  </div>
  <?php do_action('wpnp_after_main_cat'); ?>
  <div class="wpnp-cat-mobile">
    <select id="wpnp-cat-mobile-sel" name="wpnp-cat-mobile">
      <option value="general">World</option>
      <option value="politics">Politics</option>
      <option value="business">Business</option>
      <option value="entertainment">Entertainment</option>
      <option value="sports">Sports</option>
      <option value="health">Health</option>
      <option value="science">Science</option>
      <option value="technology">Technology</option>
      <option value="my-articles">My Articles</option>
    </select>
  </div>
  <?php do_action('wpnp_after_mobile_cat'); ?>
  <div id="wpnp-content-container">
    <div id='wpnp-country-container'></div>
    <div id='wpnp-news-container'></div>
    <?php do_action('wpnp_after_news_container'); ?>
  <!-- <div id="wpnp-load-more">Load more..</div> -->
  </div>
</div>
