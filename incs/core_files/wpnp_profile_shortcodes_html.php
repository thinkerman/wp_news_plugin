<?php
if (is_user_logged_in()) {
  global $wpdb;
  $table_name = $wpdb->prefix.'wpnp';
  $user = get_current_user_id();
  $sql = "SELECT * FROM $table_name WHERE user=$user ORDER BY id DESC";
  $query = $wpdb->get_results($sql, OBJECT_K);
  echo "<div id='wpnp-news-container-profile'>";
  if(!$query){
    echo "No saved Articles";
  }
  foreach ($query as $key) {
    ?>
    <div class='wpnp-tester' id='<?php echo $key->id; ?>'>
    <a href="<?php echo $key->url; ?>" rel="<?php echo $key->author; ?>" target="_blank" title="<?php $key->title; ?>"><div class='wpnp-featured-img' style='
    height:200px;
    width:100%;
    background-image:url("<?php echo $key->img; ?>");
    background-size:cover;
    background-position:center;'><span class='wpnp-cat-inner'><i class="fa fa-internet-explorer fa-lg" aria-hidden="true"></i> <?php echo $key->source; ?></span></div></a>
    <div class='wpnp-single-news'>
      <div class='wpnp-news-item wpnp-title'><span><i class="fa fa-book" aria-hidden="true"></i></span> <span class='title'> <?php echo $key->title; ?></span></div><hr>
      <div class='wpnp-news-item wpnp-author'><i class="fa fa-user fa-lg" aria-hidden="true"></i>  <?php echo $key->author; ?> </div>
      <span class='wpnp-news-item wpnp-published'><i class="fa fa-calendar fa-lg" aria-hidden="true"></i>  <?php echo $key->time; ?></span>
      <div class='wpnp-news-item wpnp-summary'><?php echo $key->summary; ?>...</div>
      <div class='wpnp-news-item wpnp-more'><a href='<?php echo $key->url; ?>' rel="author" target="_blank">Read more </a></div>
      <div class='wpnp-news-item wpnp-share'>
        <span class ="wpnp-facebook-share" data-url='<?php echo $key->url; ?>'><i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i></span>
        <span class="wpnp-twitter-share"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></span>
        <span class="wpnp-delete" data-id='<?php echo $key->id; ?>'><i class="fa fa-trash" aria-hidden="true"></i></span>
      </div>
    </div>
    </div>
    <?
  }
  echo "</div>";
}else{
  echo "Please <a href='".wp_login_url()."'>login</a> to save articles";

}

?>
