var gulp = require('gulp');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
const minify = require('gulp-minify');

gulp.task('styles', function(){
    return gulp.src(['styles/**/*.scss'])
        .pipe(plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('styles/comp/'))
        .pipe(rename({suffix: '.min'}))

});
gulp.task('default', function(){
    return gulp.watch("styles/**/*.scss", gulp.series('styles'));
    // gulp.watch("assets/js/*.js", ['es-js']);
});
