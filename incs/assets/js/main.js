jQuery(document).ready(function ($) {
  'use strict';
	//get all vars from php
	const wpnpObj = {
		au: wpnp.ajax_url, // Ajax URL
		pi: wpnp.placeholder_image,
    np: wpnp.post_per_page,
    us: wpnp.user_save,
    up: wpnp.user_profile_url,
		ai: wpnp.author_icon,
		tt: wpnp.title_icon,
		pb: wpnp.published_icon,
		sr: wpnp.source_icon,
		ak: wpnp.apiKey,
		pp: wpnp.post_per_page,
		fb: wpnp.facebook_icon,
		tw: wpnp.twitter_icon,
		lv: wpnp.favourite_icon,
    dl: wpnp.unlike_icon,
		sp: wpnp.spinner_img,
    ui: wpnp.user_id,
    ff: wpnp.fav_func,
    dn: wpnp.del_func,
		erMsg: 'Error retrieving news! :-(',
	}


	//Added all APIs as object properties
	//countrycode =  API to get the list of the countries
	//nl: NewsLocal is the object containing News API properties where URL is the base URL,
	// totalResults = number of news to get
	// apiKey =  the API key gotten from http://newsapi.org
	//countries  = Base URL for Country list API
	const api = {
		countrycode: 'http://ip-api.com/json',
		nl: {
			url: `https://newsapi.org/v2/top-headlines?pageSize=${wpnpObj.pp}&country=`,
			kwUrl: 'https://newsapi.org/v2/top-headlines?q=',
			totalResults: `&pageSize=${wpnpObj.pp}`,
			apiKey: `&apiKey=${wpnpObj.ak}`
		},
		countries: 'https://restcountries.eu/rest/v2/all',
	}
	//Ajax call tp get the list of all the known countries of the world
	const countryList = $.get(api.countries).error(()=>{console.log('WPNP is unable to load the list of countries!');});
	//Country list success callback to be executed after the country list has been retrieved from the API
	const countryListSuccess = (data) => {
		for (var country in data) {
			if (data.hasOwnProperty(country)) {
				// append all countries to Sountry Select element
				$('#wpnp-country-select').append($("<option></option>").attr("value", data[country].alpha2Code).text(data[country].name));
			}
		}
	}
	countryList.then(countryListSuccess);
	// get country code
	const getCountryCode = $.get(api.countrycode).error(() => {$('#wpnp-news-container').append(`${wpnpObj.sp}`)});

	const getCountryCodeCallBack = data => {
		//cc = country code, cn = country name. "Var" because it's a global variable
		var cc = data.countryCode.toLowerCase();
		var cn = data.country;
    //get local news
		const ln = $.get(api.nl.url + cc + api.nl.apiKey).error($('#wpnp-news-container').append(`${wpnpObj.sp}`));
		//news success callback
		const newsSuccess = res => {
			if (res.totalResults == 0) {
				// If no results found Append message
				$('#wpnp-news-container').append("<p>No news item found in this category!</p>")
			} else {
				$('#wpnp-news-container').empty();
				$(`#wpnp-country-select option[value=${cc.toUpperCase()}]`).attr('selected', 'selected');
			}
			// $('#wpnp-country').attr('data-api', cn.toLowerCase());
			const obj = res.articles
			for (var item in obj) {
				if (obj.hasOwnProperty(item)) {
					const author = obj[item].author ? obj[item].author : "Unknown Author";
					const title = obj[item].title;
					const summary = obj[item].description ? obj[item].description : "This news has no summary please click Read More to visit the source.";
					const published = obj[item].publishedAt;
					const source = obj[item].source.name;
					const url = obj[item].url;
					const img = obj[item].urlToImage ? obj[item].urlToImage : wpnpObj.pi;
					const style = {
						h: 200,
						w: 100,
						bi: img,
						bs: 'cover',
						bp: 'center'
					}
					const titleLenght = title => title.lenght > 50 ? title.substring(0, 20) + '...' : title
					$('#wpnp-news-container').append(`
            <div class='wpnp-tester'>
            <a href="${url}" rel="author" target="_blank" title="${title}"><div class='wpnp-featured-img' style='height:${style.h}px;width:${style.w}%;background-image:url("${style.bi}");
            background-size:${style.bs};background-position:${style.bp}'><span class='wpnp-cat-inner'>${wpnpObj.sr} ${source}</span></div></a>
            <div class='wpnp-single-news'>
              <div class='wpnp-news-item wpnp-title'><span>${wpnpObj.tt}</span> <span class='title'> ${titleLenght(title)}</span></div><hr>
              <div class='wpnp-news-item wpnp-author'>${wpnpObj.ai}  ${author.substring(0,20)} </div>
              <span class='wpnp-news-item wpnp-published'>${wpnpObj.pb}  ${published}</span>
              <div class='wpnp-news-item wpnp-summary'>${summary.substring(0,90)}...</div>
              <div class='wpnp-news-item wpnp-more'><a href='${url}' rel="author" target="_blank">Read more </a></div>
              <div class='wpnp-news-item wpnp-share'>
                <span class ="wpnp-facebook-share" data-url='${url}' data-img='${img}'>${wpnpObj.fb}</span>
                <span class="wpnp-twitter-share" data-url='${url}' data-img='${img}' data-title='${titleLenght(title)}'>${wpnpObj.tw}</span>
                <span class="wpnp-favourite" data-url='${url}' data-img='${img}' data-time='${published}' data-title='${titleLenght(title)}' data-summary='${summary.substring(0,90)}', data-author= '${author}' data-source='${source}'>${wpnpObj.lv}</span>
              </div>
            </div>
            </div>
          `);
				}
			}
		}
		ln.then(newsSuccess);
		const newsResLoad = (el, ajaxCall, loaded = true) => {
			if (!loaded) {
				$('#wpnp-news-container').fadeOut(1600, e => {
					$('#wpnp-news-container').empty();
					if (el === 'country') {
						$('#wpnp-country').addClass('cat-active');
						$('#wpnp-general').removeClass('cat-active');
						$('#wpnp-politics').removeClass('cat-active');
						$('#wpnp-business').removeClass('cat-active');
						$('#wpnp-entertainment').removeClass('cat-active');
						$('#wpnp-sports').removeClass('cat-active');
						$('#wpnp-health').removeClass('cat-active');
						$('#wpnp-science').removeClass('cat-active');
						$('#wpnp-tech').removeClass('cat-active');
						ajaxCall.then(newsSuccess);
						$('#wpnp-news-container').fadeIn(1600);
					} else if (el === 'general') {
						$('#wpnp-country').removeClass('cat-active');
						$('#wpnp-general').addClass('cat-active');
						$('#wpnp-politics').removeClass('cat-active');
						$('#wpnp-business').removeClass('cat-active');
						$('#wpnp-entertainment').removeClass('cat-active');
						$('#wpnp-sports').removeClass('cat-active');
						$('#wpnp-health').removeClass('cat-active');
						$('#wpnp-science').removeClass('cat-active');
						$('#wpnp-tech').removeClass('cat-active');
						ajaxCall.then(newsSuccess);
						$('#wpnp-news-container').fadeIn(1600);
					} else if (el === 'politics') {
						$('#wpnp-country').removeClass('cat-active');
						$('#wpnp-general').removeClass('cat-active');
						$('#wpnp-politics').addClass('cat-active');
						$('#wpnp-business').removeClass('cat-active');
						$('#wpnp-entertainment').removeClass('cat-active');
						$('#wpnp-sports').removeClass('cat-active');
						$('#wpnp-health').removeClass('cat-active');
						$('#wpnp-science').removeClass('cat-active');
						$('#wpnp-tech').removeClass('cat-active');
						ajaxCall.then(newsSuccess);
						$('#wpnp-news-container').fadeIn(1600);

					} else if (el === 'business') {
						$('#wpnp-country').removeClass('cat-active');
						$('#wpnp-general').removeClass('cat-active');
						$('#wpnp-politics').removeClass('cat-active');
						$('#wpnp-business').addClass('cat-active');
						$('#wpnp-entertainment').removeClass('cat-active');
						$('#wpnp-sports').removeClass('cat-active');
						$('#wpnp-health').removeClass('cat-active');
						$('#wpnp-science').removeClass('cat-active');
						$('#wpnp-tech').removeClass('cat-active');
						ajaxCall.then(newsSuccess);
						$('#wpnp-news-container').fadeIn(1600);
					} else if (el === 'entertainment') {
						$('#wpnp-country').removeClass('cat-active');
						$('#wpnp-general').removeClass('cat-active');
						$('#wpnp-politics').removeClass('cat-active');
						$('#wpnp-business').removeClass('cat-active');
						$('#wpnp-entertainment').addClass('cat-active');
						$('#wpnp-sports').removeClass('cat-active');
						$('#wpnp-health').removeClass('cat-active');
						$('#wpnp-science').removeClass('cat-active');
						$('#wpnp-tech').removeClass('cat-active');
						ajaxCall.then(newsSuccess);
						$('#wpnp-news-container').fadeIn(1600);
					} else if (el == 'sports') {
						$('#wpnp-country').removeClass('cat-active');
						$('#wpnp-general').removeClass('cat-active');
						$('#wpnp-politics').removeClass('cat-active');
						$('#wpnp-business').removeClass('cat-active');
						$('#wpnp-entertainment').removeClass('cat-active');
						$('#wpnp-sports').addClass('cat-active');
						$('#wpnp-health').removeClass('cat-active');
						$('#wpnp-science').removeClass('cat-active');
						$('#wpnp-tech').removeClass('cat-active');
						ajaxCall.then(newsSuccess);
						$('#wpnp-news-container').fadeIn(1600);
					} else if (el === 'health') {
						$('#wpnp-country').removeClass('cat-active');
						$('#wpnp-general').removeClass('cat-active');
						$('#wpnp-politics').removeClass('cat-active');
						$('#wpnp-business').removeClass('cat-active');
						$('#wpnp-entertainment').removeClass('cat-active');
						$('#wpnp-sports').removeClass('cat-active');
						$('#wpnp-health').addClass('cat-active');
						$('#wpnp-science').removeClass('cat-active');
						$('#wpnp-tech').removeClass('cat-active');
						ajaxCall.then(newsSuccess);
						$('#wpnp-news-container').fadeIn(1600);
					} else if (el === 'science') {
						$('#wpnp-country').removeClass('cat-active');
						$('#wpnp-general').removeClass('cat-active');
						$('#wpnp-politics').removeClass('cat-active');
						$('#wpnp-business').removeClass('cat-active');
						$('#wpnp-entertainment').removeClass('cat-active');
						$('#wpnp-sports').removeClass('cat-active');
						$('#wpnp-health').removeClass('cat-active');
						$('#wpnp-science').addClass('cat-active');
						$('#wpnp-tech').removeClass('cat-active');
						ajaxCall.then(newsSuccess);
						$('#wpnp-news-container').fadeIn(1600);
					} else if (el === 'tech') {
						$('#wpnp-country').removeClass('cat-active');
						$('#wpnp-general').removeClass('cat-active');
						$('#wpnp-politics').removeClass('cat-active');
						$('#wpnp-business').removeClass('cat-active');
						$('#wpnp-entertainment').removeClass('cat-active');
						$('#wpnp-sports').removeClass('cat-active');
						$('#wpnp-health').removeClass('cat-active');
						$('#wpnp-science').removeClass('cat-active');
						$('#wpnp-tech').addClass('cat-active');
						ajaxCall.then(newsSuccess);
						$('#wpnp-news-container').fadeIn(1600);
					}

				});
			}
		}
		//get clicked category
		$('#wpnp-country-select').change(function (event) {
			cc = $(this).val().toLowerCase();
			const bn = $.get(api.nl.url + cc + '&category=politics' + api.nl.apiKey);
			$('.wpnp-cat-mobile-sel option[value=business]').attr('selected', 'selected');
			//console.log(api.nl.politics + cc + '&'+ att + api.nl.apiKey);
			newsResLoad('politics', bn, false)
		});
		$('.wpnp-cat-li').click(function (e) {
			e.preventDefault();
			$('.wpnp-cat-mobile-sel option[value=general]').attr('selected', 'selected');
			$(`#wpnp-country-select option[value=${cn}]`).attr('selected', 'selected');
			if (($(this).attr('data-api')) === 'general') {
				const att = $(this).attr('data-api');
				const gn = $.get(api.nl.url + 'us' + '&category=' + att + api.nl.apiKey);
				newsResLoad('politics', gn, false)
			}
			if (($(this).attr('data-api')) === 'politics') {
				$('.wpnp-cat-mobile-sel option[value=politics]').attr('selected', 'selected');
				const att = $(this).attr('data-api');
				const pn = $.get(api.nl.url + cc + '&category=' + att + api.nl.apiKey);
				newsResLoad('politics', pn, false);
			}
			if (($(this).attr('data-api')) === 'business') {
				const att = $(this).attr('data-api');
				const bn = $.get(api.nl.url + cc + '&category=' + att + api.nl.apiKey);
				$('.wpnp-cat-mobile-sel option[value=business]').attr('selected', 'selected');
				console.log($('.wpnp-cat-mobile-sel :selected').val());
				//console.log(api.nl.politics + cc + '&'+ att + api.nl.apiKey);
				newsResLoad('business', bn, false);

			}
			if (($(this).attr('data-api')) === 'entertainment') {
				const att = $(this).attr('data-api');
				const en = $.get(api.nl.url + cc + '&category=' + att + api.nl.apiKey);
				$('.wpnp-cat-mobile-sel option[value=entertainment]').attr('selected', 'selected');
				newsResLoad('entertainment', en, false);
			}
			if (($(this).attr('data-api')) === 'sports') {
				const att = $(this).attr('data-api');
				const spn = $.get(api.nl.url + cc + '&category=' + att + api.nl.apiKey);
				$('.wpnp-cat-mobile-sel option[value=sports]').attr('selected', 'selected');
				newsResLoad('sports', spn, false);
			}
			if (($(this).attr('data-api')) === 'health') {
				const att = $(this).attr('data-api');
				const hn = $.get(api.nl.url + cc + '&category=' + att + api.nl.apiKey);
				$('.wpnp-cat-mobile-sel option[value=health]').attr('selected', 'selected');
				newsResLoad('health', hn, false);
			}
			if (($(this).attr('data-api')) === 'science') {
				const att = $(this).attr('data-api');
				const sn = $.get(api.nl.url + cc + '&category=' + att + api.nl.apiKey);
				$('.wpnp-cat-mobile-sel option[value=science]').attr('selected', 'selected');
				newsResLoad('science', sn, false);
			}
			if (($(this).attr('data-api')) === 'technology') {
				const att = $(this).attr('data-api');
				const tn = $.get(api.nl.url + cc + '&category=' + att + api.nl.apiKey);
				$('.wpnp-cat-mobile-sel option[value=technology]').attr('selected', 'selected');
				newsResLoad('tech', tn, false);
			}
		})
		//perform live search
		const searchFunc = (e) => {
			let searchVal = $('#wpnp-search-input').val();
			const cc = $('#wpnp-country-select').val().toLowerCase();
			if (searchVal == "") {
				const bn = $.get(api.nl.url + cc + '&category=politics' + api.nl.apiKey);
				$('#wpnp-country-wrapper').fadeIn(1600)
				$('#wpnp-search').css({"padding-bottom": "0"})
				newsResLoad('politics', bn, false);
			} else {
				const kw = $.get(api.nl.kwUrl + searchVal + api.nl.apiKey);
				$('#wpnp-country-wrapper').fadeOut(1600)
				$('#wpnp-search').css({"padding-bottom": "20px"})
        newsResLoad('politics', kw, false);
			}
		}
		$('#wpnp-search-input').keypress(e => {	e.which !== 0 ? searchFunc(e) : null; }) //call live search function
		//Live search ends
	}
	//on change mobile category
	$('#wpnp-cat-mobile-sel').on('change', (event) => {
    $('#wpnp-cat-mobile-sel option:selected').text() == "World" ? $('#wpnp-general').click() : null;
    $('#wpnp-cat-mobile-sel option:selected').text() == "Politics" ? $('#wpnp-politics').click(): null;
    $('#wpnp-cat-mobile-sel option:selected').text() == "Business" ? $('#wpnp-business').click(): null;
    $('#wpnp-cat-mobile-sel option:selected').text() == "Entertainment" ? $('#wpnp-entertainment').click() : null;
    $('#wpnp-cat-mobile-sel option:selected').text() == "Sports" ? $('#wpnp-sports').click(): null;
    $('#wpnp-cat-mobile-sel option:selected').text() == "Health" ? $('#wpnp-health').click(): null;
    $('#wpnp-cat-mobile-sel option:selected').text() == "Science" ? $('#wpnp-science').click(): null;
    $('#wpnp-cat-mobile-sel option:selected').text() == "Technology" ? $('#wpnp-tech').click(): null;
    if ($('#wpnp-cat-mobile-sel option:selected').text() == "My Articles") {
      if (!wpnpObj.up == " ") {
        window.location.href= wpnpObj.up;
      }else{
        alert("This page is not enabled")
      }
    }

	});
	//get country code promise
	getCountryCode.then(getCountryCodeCallBack)
// facebook api wrapper
  $(document).ready(function(){
    $.ajaxSetup({ cache: true });
    $.getScript('https://connect.facebook.net/en_US/sdk.js', () => {
      FB.init({
        appId: '1796435363756263',
        version: 'v2.7'
      });
    });
  })
  $(document).on("click",".wpnp-facebook-share", (e) => {
     const url = e.target.parentElement.dataset.url == undefined ? e.currentTarget.dataset.id :  e.target.parentElement.dataset.url;
     const img = e.target.parentElement.dataset.img
     console.log(img);
      FB.ui({
          method: 'share',
          href: url,
          }, function(response){});
  });
  $(document).on("click",".wpnp-twitter-share", (e) => {
     const url = e.target.parentElement.dataset.url == undefined ? e.currentTarget.dataset.id :  e.target.parentElement.dataset.url;
     const img = e.target.parentElement.dataset.img
     // const text = e.target.parentElement.dataset.title
     const text = "Shared from Wordpress News Plugin."
     window.open(`http://twitter.com/share?text=${text}&url=${url}`, '_blank')
  });
  $(document).on("click",'.wpnp-favourite', (e) =>{
      e.preventDefault();
      const title = e.target.parentElement.dataset.title
      const img = e.target.parentElement.dataset.img
      const post_url = e.target.parentElement.dataset.url
      const summary = e.target.parentElement.dataset.summary
      const author = e.target.parentElement.dataset.author
      const source = e.target.parentElement.dataset.source;
      const time = e.target.parentElement.dataset.time
      const url = wpnpObj.au;
      const data = {
        action: wpnpObj.ff,
        'savefav': true,
        'title': title,
        'time': time,
        'img_url': img,
        'summary': summary,
        "post_url": post_url,
        'author': author,
        'source': source,
        'user_id': wpnpObj.ui
      };
      const success = data =>{alert(data)};
      $.post(url,data, success)
  });
  $('.wpnp-delete').on('click',(e)=>{
    e.preventDefault();
    const url = wpnpObj.au;
    const delete_data = {
      action: wpnpObj.ff,
      'delete': true,
      'id': e.currentTarget.dataset.id
    };
    const success = data =>{
      alert(data)
      window.location.reload()
    };
    const err = data =>{
      console.log(data);
    }
      $.post(url,delete_data,success).fail(err);
  })
  $('#wpnp-user-profile').click((e) => {
    e.preventDefault();
    if (!wpnpObj.up == " ") {
      window.location.href= wpnpObj.up;
    }else{
      alert("This page is not enabled")
    }
  })
})
