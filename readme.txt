Wordpress World News plugin is a plugin that simplifies displaying world news on your website.

This plugins utilizes 3 APIs
1. https://newsapi.org/
2. https://restcountries.eu/rest/v2/all
3. http://ip-api.com/json
4. Facebook API

https://newsapi.org/ API is used to obtain news articles around the world, this API does not collect any user personal
and will only display user-specified news or general news across the world. To find out more about this plugin please
check https://newsapi.org/

https://restcountries.eu/rest/v2/all API is used to obtain the list of the countries in the world. This is utilized in
order to shorten the lenght of the code of the plugin and produce cleaner more maintanable code. To find out more about
this plugin please check https://restcountries.eu/

http://ip-api.com/json is used to obtain current User location in other to serve the user news articles from User's
current location. This API collects information about the user's present location and uses the information in a transparent
manner.

Facebook API is used to publish news articles on a user's facebook page/group. This API call strictly on the server side
and has nothing to do with the user except to obtain the persmission to post to the user's facebook page.


No user's personal information is stored either by the plugin or the APIs utilized by the plugin (except for API 1 above
in which case it is necessary in order to obtain an API key to use with WP-news-plugin)

Plugin git directory is https://gitlab.com/thinkerman/wp_news_plugin.

How to use Wordpress News Plugins

The plugin functions in a simple manner and uses shortcodes to display it's content. After obtaining an API key from
API 1. above, enter the API key in Dashboard ->WP News Plugin "Enter API Key here" textbox. Please note, you are limited
to 1000 calls in a day which means that if you have frequent visitors on your website, it is advisable you don't register
for the free version as users will get an error message saying you've made too much calls within the last 24 hours.

A subsequent version of this plugin being worked on which will be a paid version in order to offset the bills of unlimited
calls to the API.

The plugin also offers the user the ability to choose how many news articles to display on load. Note, the default is 9
articles. It is recommended that users don't display more than 15 articles on load to improve page load speed.

This plugin also has a few actions for developers who might be interested in adding a few perks to it. The actions are
strategically placed and are self-explanatory.

I hope you enjoy this plugin made in my free time and hope you download the subsequent versions too <3!